{{#if showButton}}
    <div class="infinite-scroll-btn-holder ">
        <button data-action="load-pages" class="infinite-scroll-btn-load-page" id="{{ buttonID }}">
            {{ buttonText }}
        </button>
    </div>
{{/if}}