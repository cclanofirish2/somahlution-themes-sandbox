{{#if hasBadges}}
    <div class="itembadges-container">
        <div class="itembadges-badge-container" data-view="Itembadges.List.View"></div>
    </div>
{{/if}}