<div class="custom-fields-checkout-field" id="custom-fields-checkout-{{fieldId}}" data-type="{{type}}" data-validation="control-group">
    {{#if isHeading}}
        <h3 class="custom-fields-checkout-field-heading">{{label}}</h3>
    {{else}}
        {{#if isCheckbox}}
            <div class="custom-fields-checkout-field-checkbox">
                <input type="checkbox" name="{{fieldId}}" id="custom_field_{{fieldId}}" class="custom-fields-checkout-field-input-checkbox" data-validation="control">
                <label class="custom-fields-checkout-field-label-checkbox" for="custom_field_{{fieldId}}">{{label}}</label>
            </div>
        {{else}}
            <label for="custom_field_{{fieldId}}" class="custom-fields-checkout-field-label">
                {{label}}
                {{#if isMandatory}}
                    <span class="custom-fields-checkout-field-label-required">*</span>
                {{/if}}
            </label>
            <div class="custom-fields-checkout-field-control">
                {{#if isTextArea}}
                    <textarea name="{{fieldId}}" placeholder="{{placeholder}}" id="custom_field_{{fieldId}}" {{#if isMandatory}}required{{/if}} class="custom-fields-checkout-field-input-textarea" data-validation="control"></textarea>
                {{else}}
                    {{#if isDate}}
                    <input type="date" name="{{fieldId}}" placeholder="{{placeholder}}" id="custom_field_{{fieldId}}" {{#if isMandatory}}required{{/if}} class="custom-fields-checkout-field-input-date" data-validation="control" data-field-type="date" data-format="yyyy-mm-dd">
                    {{else}}
                    <input type="text" name="{{fieldId}}" placeholder="{{placeholder}}" id="custom_field_{{fieldId}}" {{#if isMandatory}}required{{/if}} class="custom-fields-checkout-field-input-text" data-validation="control">
                    {{/if}}
                {{/if}}
            </div>
        {{/if}}
    {{/if}}
</div>
