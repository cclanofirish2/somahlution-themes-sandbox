<div id="layout" class="checkout-layout">
	<header id="site-header" class="checkout-layout-header" data-view="Header"></header>
	<div id="main-container">

		<div class="checkout-layout-breadcrumb" data-view="Global.Breadcrumb" data-type="breadcrumb"></div>
		<div class="checkout-layout-notifications">
			<div data-view="Notifications"></div>
		</div>
		<!-- Main Content Area -->
		<div class="address-edit-fields-group-input-checkbox-billschedule" data-input="custbody_so_items_use_bill_schedule">
			<div class="address-edit-fields-group-input-checkbox-billschedule-image">
				<img src="https://3604689-sb1.app.netsuite.com/core/media/media.nl?id=54010&c=3604689_SB1&h=3dc4fc4a564f0853afc6&whence=">
			</div>
			<div class="address-edit-fields-group-input-checkbox-billschedule-text">

						<!-- <input type="checkbox" id="{{manage}}isresidential" value="T" data-unchecked-value="F" name="isresidential" {{#if isAddressResidential}} checked {{/if}} >
						{{translate 'This is a Residential Address'}}
						<i class="address-edit-fields-icon-question-sign" data-toggle="tooltip" title="" data-original-title="{{translate 'Indicating that this is a residential address will help us determine the best delivery method for your items.'}}"></i>
					</label>
				</div> -->

	 			<!-- {{model.__customFieldsMetadata.custbody_so_items_use_bill_schedule.label}} -->
				<input type="checkbox" name="custbody_so_items_use_bill_schedule" value=custbody_so_items_use_bill_schedule checked>
				{{translate 'For your shopping convenience, your purchase will be split into 3 easy payments. Click here to enable single pay'}}
			</div>
		</div>
		<div id="content" class="checkout-layout-content"></div>
		<!-- / Main Content Area -->
	</div>

	<footer id="site-footer" class="checkout-layout-footer" data-view="Footer"></footer>
</div>



{{!----
The context variables for this template are not currently documented. Use the {{log this}} helper to view the context variables in the Console of your browser's developer tools.

----}}
